import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type User from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const editedUser = ref<User>({ id: -1, login: "", name: "", password: "", remain: -1 })
  const lastId = 3
  const currentUser = ref("")
  const users = ref<User[]>([
    { id: 1, login: "admin1", name: "admin 1", password: "12345678", remain: 2000 },
    { id: 2, login: "admin2", name: "admin 2", password: "Pass@5678", remain: 3000 },
  ])
  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName)
    if (index >= 0){
      const user = users.value[index];
      if (user.password === password){
        return true
      }
      return false
    }
    return false
  }
  const saveUser = () => {
    const index = users.value.findIndex((item) => item.id === editedUser.value.id);
    users.value[index].remain =  6000
  }

  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);
    users.value.splice(index, 1)
  }
  // const saveUser = () => {
  //   editedUser.value.id
  // }

  const editUser = (money: number): void => {
    currentUser.value = localStorage.getItem("loginName") || ""
    const index = users.value.findIndex((item) => item.login === currentUser.value);
    users.value[index].remain +=  money
    // editedUser.value.remain = {...user}
    saveUser();
  }

  const editUserCurrent = ():number => {
    currentUser.value = localStorage.getItem("loginName") || ""
    const index = users.value.findIndex((item) => item.login === currentUser.value);
    return users.value[index].remain
  }

  return { users, deleteUser, editedUser, login, editUser, editUserCurrent }
})
