import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const isShow = ref(false)
  const message = ref("")
  const timeOut = ref(2000)
  const showMessage = (msg: string, time_Out: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeOut.value = time_Out
  }
  const closeMessage = () => {
    message.value = "";
    isShow.value = false;
  }

  return { isShow, message, showMessage, closeMessage, timeOut }
})